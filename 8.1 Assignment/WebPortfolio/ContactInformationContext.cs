﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebPortfolio.Models;

namespace WebPortfolio
{
    public class ContactInformationContext : DbContext
    {
        public DbSet<ContactUs> Contacts { get; set; }

        public ContactInformationContext(DbContextOptions<ContactInformationContext> options)
            : base(options)
        {
            Database.EnsureCreated();
        }
    }
}
