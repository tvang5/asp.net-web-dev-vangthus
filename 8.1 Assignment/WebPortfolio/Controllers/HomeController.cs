﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace WebPortfolio.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            // Returns the index.cshtml file located in Views/Home/
            return View();
        }

        [Route("test")]
        public IActionResult test()
        {
            return new ContentResult { Content = "testing" };
        }
    } 
}