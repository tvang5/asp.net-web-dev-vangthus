﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebPortfolio.Models;

namespace WebPortfolio.Controllers
{
    [Route("ContactUs")]
    public class ContactController : Controller
    {
        private readonly ContactInformationContext context;
        public ContactController(ContactInformationContext context)
        {
            this.context = context;
        }

        [HttpGet, Route("")]
        public IActionResult ContactUs(bool? isSuccess)
        {
            if(isSuccess == true & isSuccess != null)
            {
                ViewBag.IsSuccess = true;
                ViewBag.SuccessMessage = "Contact information successfully sent.";
            }
            ViewBag.FailureMessage = "Contact information sending failed.";


            // Displays page where users can enter in contact information.
            return View();
        }

        [HttpPost, Route("")]
        public IActionResult ContactUs(ContactUs contact)
        {

            if (!ModelState.IsValid)
            {
                return View();
            }

            contact.DateSubmitted = DateTime.Now;

            this.context.Add(contact);
            this.context.SaveChanges();

            bool successValue = true;

            return RedirectToAction("ContactUs", "Contact", new
            {
                isSuccess = successValue
            });
        }

        [Route("/ContactRequests")]
        public IActionResult ContactRequests()
        {
            ContactRequests cr = new ContactRequests();
            cr.Requests = this.context.Contacts.ToList();
            return View(cr);
        }
    }
}