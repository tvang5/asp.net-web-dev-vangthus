﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace WebPortfolio.Controllers
{
    [Route("resume")]
    public class ResumeController : Controller
    {
        [Route("")]
        public IActionResult Resume()
        {
            // Returns the resume.cshtml file located in Views/Resume/
            return View();
        }

        [Route("test")]
        public IActionResult test()
        {
            return new ContentResult { Content = "testing2" };
        }
    }
}