importScripts('/index.js');

self.addEventListener('install', function (e) {
    e.waitUntil(
        caches.open('airhorner').then(function (cache) {
            return cache.addAll([
                '/',
                '/index.html',
                '/contact.html',
                '/projects.html',
                '/resume.html',
                '/js/bootstrap.min.js',
                '/js/jquery-2.1.4.min.js',
                '/js/script.js',
                '/js/shapesProject.js',
                '/js/mediaProject.js',
                '/css/bootstrap-theme.min.css',
                '/css/bootstrap.min.css',
                '/css/styles.css',
                '/images/coins.jpg',
                '/media/chart.jpg',
                '/media/test.mp4'
            ]);
        })
    );
});

self.addEventListener('fetch', function (event) {
    console.log(event.request.url);

    event.respondWith(
        caches.match(event.request).then(function (response) {
            return response || fetch(event.request);
        })
    );
});