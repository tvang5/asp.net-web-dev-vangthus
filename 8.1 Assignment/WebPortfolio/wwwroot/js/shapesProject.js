var canvas, context;
canvas = document.getElementById("shapeCanvas");

// if(canvas && canvas.getContext){
//     context = canvas.getContext('2d');

//     // begin listening to instructions
//     context.beginPath();

//     // where to start the drawing 75 x axis, 50 y axis
//     context.moveTo(75, 50);
//     context.lineTo(75, 100);
//     context.lineTo(25, 100);
//     context.fill();
// }

// if(canvas && canvas.getContext){
//     context = canvas.getContext('2d');

//     // begin listening to instructions
//     context.beginPath();

//     // where to start the drawing 75 x axis, 50 y axis
//     context.moveTo(100, 100);
//     context.lineTo(100, 300);
//     context.lineTo(150, 300);
//     context.lineTo(150, 155);
//     context.lineTo(205, 155);
//     context.lineTo(205, 100);
//     context.closePath();
//     context.fillStyle = '#0099ff';
//     context.fill();

//     context.lineWidth = 6;
//     context.lineJoin = 'round';
//     context.strokeStyle = '#cccccc';
//     context.stroke();
// }

if(canvas && canvas.getContext){
    context = canvas.getContext('2d');

    // redbox
    context.fillStyle = "rgb(500, 0, 0)";
    context.fillRect(350, 350, 100, 100);

    // blue box (transparency)
    context.fillStyle = "rgba(0, 0, 500, .5)";
    context.fillRect(400, 400, 100, 100);

    // clear box
    context.clearRect(415, 415, 20, 20);

    // green outline
    context.strokeStyle = 'rgb(51, 153, 0)';
    context.lineWidth = 6;
    context.strokeRect(415, 415, 20, 20);
}

if (canvas && canvas.getContext) {
    context = canvas.getContext('2d');

    var g1 = context.createRadialGradient(160, 120, 0, 320, 200, 300);
    g1.addColorStop(0, '#ffffff');
    g1.addColorStop(1, '#999999');

    context.lineWidth = 0;
    context.strokeStyle = '#000000';
    context.fillStyle = g1;
    context.beginPath();
    context.arc(180, 180, 160, 0, Math.PI * 2, true);
    context.fill();

    var g2 = context.createRadialGradient(360, 320, 0, 260, 220, 200);
    g2.addColorStop(0, '#ffffff');
    g2.addColorStop(1, '#999999');

    // inner circle
    context.fillStyle = g2;
    context.beginPath();
    context.arc(180, 180, 130, 0, Math.PI * 2, true);
    context.fill();

    // text
    context.fillStyle = '#ffffff';
    context.font = '280px Arial';
    context.fillText('N', 80, 280);
}

// if(canvas && canvas.getContext){
//     context = canvas.getContext("2d");
//     var img = new Image();
//     img.onload = function(){
//         context.drawImage(img, 0, 0);
//     }
//     img.src = "media/coins.jpg";

//     context.scale(0.5, 0.5);
// }

// var degreesToRadians = function(degrees){
//     var radians = (degrees * (Math.PI / 180));
//     return radians;
// };
// if(canvas && canvas.getContext){
//     context = canvas.getContext("2d");
//     var img = new Image();
//     img.onload = function(){
//         context.drawImage(img, 0, 0);
//     }
//     img.src = "media/coins.jpg";

//     //context.rotate(0.2);
//     context.rotate(degreesToRadians(-15));
// }


// if(canvas && canvas.getContext){
//     context = canvas.getContext("2d");

//     context.translate(50, 50);
//     var text, 
//     img = new Image();

//     img.onload = function(){
//         context.drawImage(img, 0, 0);
//         context.fillStyle = '#ffffff';
//         context.strokeStyle = '#000000';
//         context.lineWidth = 6;
//         text = "Cool Lab";
//         context.font = '3em Arial';
//         context.strokeText(text, 75, 55);
//         context.fillText(text, 75, 55);

//         text = "For your IT Course Help";
//         context.font = '2em Arial';
//         context.strokeText(text, 15, 200);
//         context.fillText(text, 15, 200);
//     }
//     img.src = "media/coins.jpg"; 
// }

//// Draw a line
// var x = 0,
//     y = 0,
//     frame,
//     context = canvas.getContext('2d');

// var draw = function () {
//     if (x <= canvas.width) {
//         context.clearRect(0, 0, 345, 345);
//         context.strokeStyle = 'rgb(139,0,0)';
//         context.lineWidth = 8;
//         context.beginPath();
//         context.moveTo(0, 0);
//         context.lineTo(x += 10, y += 10);
//         context.stroke();
//     } else {
//         clearInterval(frame);
//         console.log("Animation Done");
//     }
// }

// if (canvas && canvas.getContext) {
//     frame = setInterval(function () {
//         draw();
//     }), 25;
// }

//clipping/state management
if(canvas && canvas.getContext){
    context = canvas.getContext('2d');
    context.fillStyle = 'rgba(153, 153, 0.75)';
    context.strokeStyle = '#999999';
    context.lineWidth = 5;
    context.lineCap = 'round'

    // Take snapshot
    context.save();
    context.beginPath();
    context.moveTo(600, 105);
    context.lineTo(650, 250);
    context.lineTo(762, 300);
    context.lineTo(854, 10);
    context.closePath();
    context.stroke();
    context.clip();
    context.fillRect(0, 0, 215, 215);
    context.restore();
    context.fillRect(650, 150, 50, 50);
}

// if(canvas && canvas.getContext){
//     context = canvas.getContext('2d');
//     context.fillStyle = 'rgba(153, 153, 153, 0.75)';
//     context.strokeStyle = '#999';
//     context.lineWidth = 5;
//     context.lineCap = 'round';

//     context.save();

//     context.rect(0, 0, 100, 100);
//     context.rect(105, 0, 100, 100);
//     context.clip();
//     context.stroke();

//     context.fillRect(0, 0, 300, 300);

//     context.restore();

//     context.fillRect(0, 105, 100, 100);
// }

// var canvas = document.getElementById('canvas'),
//     img = new Image(),
//     context = null,
//     dataUrl = null,
//     isMagnified = false,

//     init = function () {
//         img.onload = function () {
//             context.drawImage(img, 0, 0);
//             dataUrl = canvas.toDataURL();
//         }
//         img.src = 'media/coins.jpg';
//         isMagnified = false;
//     },

//     magnify = function() {
//         context.save();
//         context.lineWidth = 10;
//         context.shadowColor = '#000';
//         context.shadowBlur = 15;
//         context.shadowOffsetX = 5;
//         context.shadowOffsetY = 5;

//         context.save();
//         context.beginPath();
//         context.moveTo(230, 230);
//         context.lineCap = 'round';
//         context.lineWidth = 30;
//         context.lineTo(285, 285);
//         context.stroke();

//         context.beginPath();
//         context.arc(150, 150, 115, 0, Math.PI * 2, true);
//         context.clip();

//         var magnified = new Image();
//         magnified.src = dataUrl;

//         context.scale(1.5, 1.5);
//         context.drawImage(img, -40, -40);
//         context.restore();

//         isMagnified = true;
//     }

// if (canvas && canvas.getContext) {
//     context = canvas.getContext('2d');

//     init();

//     $(canvas).click(function () {
//         if (isMagnified) {
//             init();
//         } else {
//             magnify();
//         }
//     });
// }


if (canvas && canvas.getContext) {
    context = canvas.getContext('2d');
    var img = new Image();
    img.onload = function () {
        context.drawImage(img, 0, 1500);
        context.beginPath();
        context.lineTo(70, 1500);
        context.lineTo(200, 1500);
        context.lineTo(250, 2000);
        context.lineTo(452, 1750);
        context.lineTo(695, 2500);
        context.lineTo(850, 2200);
        context.lineTo(1000, 1500);
        context.stroke();
    }
    // need a grid image
    img.src = 'media/chart.jpg';
    context.scale(0.3, 0.3);
    context.strokeStyle = 'rgb(31, 172, 242)';
}

// var chartData = [{
//         month: 70,
//         perf: 105
//     },
//     {
//         month: 105,
//         perf: 280
//     },
//     {
//         month: 140,
//         perf: 115
//     },
//     {
//         month: 175,
//         perf: 45
//     },
//     {
//         month: 210,
//         perf: 200
//     },
//     {
//         month: 245,
//         perf: 245
//     },
//     {
//         month: 280,
//         perf: 177
//     }
// ];

// canvas = document.getElementById('canvas'),
// context = canvas.getContext('2d'),
// index = 0,

//     drawSegment = function () {
//         var x1, y1, x2, y2;

//         x1 = chartData[index].month;
//         y1 = chartData[index].perf;

//         x2 = chartData[index + 1].month;
//         y2 = chartData[index + 1].perf;

//         context.beginPath();
//         context.moveTo(x1, y1);
//         context.lineTo(x2, y2);
//         context.stroke();

//         index++;
//     };

// if (canvas && canvas.getContext) {
//     var bkgImg = new Image();
//     bkgImg.onload = function () {
//         context.drawImage(bkgImg, 0, 0);
//         drawSegment();
//     }
//     // need a grid image
//     bkgImg.src = 'media/coins.jpg';

//     context.strokeStyle = 'rgb(31, 172, 242)';
//     context.lineWidth = 4;
//     context.lineCap = 'round';

//     var frame = setInterval(function(){
//         drawSegment();

//         if(!(index < chartData.length - 1)){
//             clearInterval(frame);
//         }
//     }, 750);
// }