﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebPortfolio.Models;

namespace WebPortfolio
{
    public class ContactRequests
    {
        public List<ContactUs> Requests { get; set; }

        public ContactRequests()
        {
            this.Requests = new List<ContactUs>();
        }
    }
}
