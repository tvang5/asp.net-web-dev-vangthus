﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebPortfolio.Models
{
    public class ContactUs
    {
        public long Id { get; set; }

        [Display(Name = "Contact Name")]
        [Required]
        [DataType(DataType.Text)]
        public string Name { get; set; }

        [Display(Name = "Email")]
        [Required]
        public string Email { get; set; }

        [Display(Name = "Phone Number")]
        public string Phone { get; set; }
        
        public DateTime DateSubmitted { get; set; }

        [Display(Name = "Message")]
        [Required]
        public string Message { get; set; }
    }
}
